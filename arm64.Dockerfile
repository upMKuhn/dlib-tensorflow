# This is a sample Dockerfile you can modify to deploy your own app based on face_recognition on the GPU
# In order to run Docker in the GPU you will need to install Nvidia-Docker: https://github.com/NVIDIA/nvidia-docker

# https://gist.github.com/JasonAtNvidia/e03e6675849d1d4049b85ea41efb2171

FROM nvcr.io/nvidia/l4t-base:r32.3.1
ARG TF_VERSION=v1.14.0
ARG ARCH=arm64
ARG DLIB_VERSION=19.19
ARG PYTHON_VERSION=python-3.7

RUN apt update \
    && apt install -y software-properties-common \
    && add-apt-repository -y ppa:deadsnakes/ppa


RUN apt install -y \ 
    build-essential \
    gcc \
    g++ \
    curl \
    git \
    pkg-config \
    zip \
    zlib1g-dev \
    unzip \
    cmake \
    libopenblas-base \
    libopenblas-dev \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender1 \
    python3.7 \
    python3-pip \
    libhdf5-dev \
    python3.7-dev \
    python3-h5py

RUN curl https://developer.download.nvidia.com/compute/redist/jp/v43/tensorflow-gpu/tensorflow_gpu-1.15.0+nv20.1-cp36-cp36m-linux_aarch64.whl --output tensorflow_gpu-1.15.0+nv20.1-cp36-cp36m-linux_aarch64.whl
RUN pip3 install cpython cython ./tensorflow_gpu-1.15.0+nv20.1-cp36-cp36m-linux_aarch64.whl
RUN rm -f /usr/bin/python3 && ln -sf /usr/bin/python3.7 /usr/bin/python3