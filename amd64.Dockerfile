# This is a sample Dockerfile you can modify to deploy your own app based on face_recognition on the GPU
# In order to run Docker in the GPU you will need to install Nvidia-Docker: https://github.com/NVIDIA/nvidia-docker

FROM ubuntu:18.04

ARG TF_VERSION=1.14.0
ARG DLIB_VERSION=v19.19

RUN apt update \
    && apt install -y software-properties-common \
    && add-apt-repository -y ppa:deadsnakes/ppa

RUN apt update && apt install -y \
    curl \
    cmake \
    gcc \
    g++ \
    git \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender1 \
    python3.7 \
    python3.7-dev \
    python3-distutils
RUN curl https://bootstrap.pypa.io/get-pip.py | python3.7

RUN pip3 install tensorflow-gpu==${TF_VERSION}


RUN git clone --single-branch --branch ${DLIB_VERSION}  https://github.com/davisking/dlib.git dlib
RUN cd dlib && python3.7 setup.py install --set DLIB_USE_CUDA=1 --set USE_AVX_INSTRUCTIONS=1
